package com.daioware.net.distributed;

import static com.daioware.net.distributed.ConnectionUtil.*;

import java.io.IOException;

import com.daioware.net.socket.UDPSender;

public class ConnectionsSender extends ConnectionManager{

	public ConnectionsSender(DistributedServer worker) {
		super(worker);
	}

	@Override
	public synchronized void run() {
		DistributedServer worker=getWorker();
		byte buffer[]=new byte[BUFF_SIZE];
		UDPSender sender=worker.getUdpElement().getSender().clone();
		println("Connection sender running");
		sender.setSrcAddr(worker.getServSocket().getAddr());
		while(isActive()) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			println("sending current data to co-workers...");
			setCurrentBufferData(worker, buffer);
			println("Sending update with number of conns:"+worker.getCurrentConnections());
			for(TcpActiveMachine machine:worker) {
				try {
					sender.setDestAddr(machine.getAddr());
					sender.send(buffer);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			println("Finished sending current data");
		}
		println("Closing sender, sending 'close message' to co-workers...");
		setCloseServerData(buffer);
		for(TcpActiveMachine machine:worker) {
			try {
				sender.setDestAddr(machine.getAddr());
				sender.send(buffer);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}	
		notify();
		println("Connection sender stop running");
	}
}
