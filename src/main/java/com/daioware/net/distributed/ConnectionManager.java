package com.daioware.net.distributed;

public abstract class ConnectionManager implements Runnable{
	protected DistributedServer worker;
	private boolean active;
	public ConnectionManager(DistributedServer worker) {
		setWorker(worker);
		setActive(true);
	}
	public DistributedServer getWorker() {
		return worker;
	}
	public void setWorker(DistributedServer worker) {
		this.worker = worker;
	}
	public boolean isActive() {
		return active;
	}
	public synchronized void setActive(boolean active) {
		this.active = active;
		notify();
		if(!active) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
	public void print(Object o) {
		worker.print(o);
	}
	public void println(Object o) {
		worker.println(o);
	}
	
}
