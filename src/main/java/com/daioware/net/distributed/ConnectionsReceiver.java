package com.daioware.net.distributed;

import static com.daioware.net.distributed.ConnectionUtil.*;

import java.io.IOException;

import com.daioware.net.socket.UDPElement;
import com.daioware.net.socket.UDPReceiver;

public class ConnectionsReceiver extends ConnectionManager {
	public static final int CLOSED_SERVER=-1;
	public static final int NEW_SERVER=-2;

	public ConnectionsReceiver(DistributedServer worker) {
		super(worker);
		setActive(true);
	}

	class InnerThread implements Runnable{
		private byte buff[];
		private String transmitterAddr;
		
		public InnerThread(String transmitterAddr,byte[] buff) {
			this.buff = buff;
			this.transmitterAddr=transmitterAddr;
		}

		@Override
		public void run() {
			TcpActiveMachine machine;
			machine=worker.get(transmitterAddr);
			if(machine!=null) {
				if(isClosedServer(buff)) {
					println("Machine "+transmitterAddr+" is being deleted from 'known machines' list");
					if(worker.remove(transmitterAddr)!=null) {
						println("Machine '"+transmitterAddr+"' removed");
					}
					for(TcpActiveMachine m:worker) {
						println(m);
					}
				}
				else {
					getCurrentBufferData(machine, buff);
					println("Updated active machine:");
				}				
			}
			else {
				println("Machine "+transmitterAddr+" is not in the 'known machines' list, trying to add");
				machine=getNewBufferData(buff);
				machine.setAddr(transmitterAddr);
				if(!worker.add(machine)) {
					println("Not added");
				}
			}
			println(machine);	
		}	
	}
	public void setActive(boolean s) {
		if(!s) {
			worker.getUdpElement().getReceiver().close();
			super.setActive(s);
		}
		else {
			super.setActive(s);
		}
	}
	@Override
	public void run() {
		UDPElement element=worker.getUdpElement();
		UDPReceiver receiver=element.getReceiver();
		byte buff[];
		println("Connection receiver manager running");
		while(isActive()) {
			buff=new byte[BUFF_SIZE];
			try {
				receiver.receive(buff);
				new Thread(new InnerThread(receiver.getTransmitterAddr(), buff)).start();
			} catch (IOException e) {
				println("Connection receiver:"+e.getMessage());
			}
		}	
		receiver.close();
		synchronized (this) {
			notify();
		}
		println("Connection receiver manager stop running");
	}
	
}
