package com.daioware.net.distributed;

import java.io.IOException;

import com.daioware.net.socket.TCPClientSocket;
import com.daioware.net.socket.TCPServSocket;
import com.daioware.net.socket.UDPElement;
import com.daioware.stream.Printer;

public class DistributedConnector extends DistributedServer{

	public DistributedConnector(UDPElement udpElement, TCPServSocket servSocket, Printer printer,
			int maxSimultaneousConnections) {
		super(udpElement, servSocket, printer,Integer.MAX_VALUE);
	}

	@Override
	public Runnable getRunnable(TCPClientSocket clientSocket) {
		return new Runnable() {		
			@Override
			public void run() {
				try {
					clientSocket.close();
				} catch (IOException e) {
					getPrinter().printStackTrace(e);
				}							
			}
		};
	}

}
