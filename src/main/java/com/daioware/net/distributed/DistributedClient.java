package com.daioware.net.distributed;

import java.io.IOException;
import java.net.UnknownHostException;

import com.daioware.net.socket.TCPClientSocket;
import com.daioware.stream.Printer;
import com.daioware.types.typeConverter.FullConverter;

public abstract class DistributedClient extends Thread{
	private TCPClientSocket clientSocket;
	private Printer printer;
	public DistributedClient(TCPClientSocket clientSocket,Printer printer) {
		this.clientSocket = clientSocket;
		this.printer=printer;
	}

	public TCPClientSocket getClientSocket() {
		return clientSocket;
	}

	public void setClientSocket(TCPClientSocket clientSocket) {
		this.clientSocket = clientSocket;
	}
	public void print(Object o) {
		printer.print(o);
	}
	public void println(Object o) {
		printer.println(o);
	}
	public TCPClientSocket getNewConnection(String addr,int port) throws UnknownHostException, IOException {
		return new TCPClientSocket(addr, port);
	}
	public abstract Runnable getRunnable(TCPClientSocket socket);
	public void run() {
		byte buff[]=new byte[250];
		String addr,prevAddr=clientSocket.getAddr();
		int port,prevPort=clientSocket.getOutputPort();
		boolean connected;
		try {
			do {
				println("Confirming connection "+prevAddr+":"+prevPort);
				println("Reading response from distributed server");
				clientSocket.readBytes(buff);
				addr=FullConverter.toString(buff);
				port=FullConverter.toInt(buff,addr.length()+Integer.BYTES);
				if(addr.equals(prevAddr) && prevPort==port) {
					println("Connection successfull, executing running method");
					new Thread(getRunnable(clientSocket)).start();
					connected=true;
				}
				else {
					println("Redirecting to "+addr+":"+port);
					clientSocket.close();
					clientSocket=getNewConnection(addr,port);
					prevPort=port;
					prevAddr=addr;
					connected=false;
				}			
			}while(!connected);
		}catch(IOException e) {
			e.printStackTrace();
		}	
	}
	public static void main(String[] args) {
		int port=6571;
		String addr;
		TCPClientSocket socket;
		addr="127.0.0.1";
		addr="192.168.1.69";
		try {
			socket=new TCPClientSocket(addr, port);
			new DistributedClient(socket,Printer.defaultPrinter) {
				@Override
				public Runnable getRunnable(TCPClientSocket socket) {
					return new Runnable() {						
						@Override
						public void run() {
							System.out.println("Executing client :)");						
						}
					};
				}			
			}.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
