package com.daioware.net.distributed;

import com.daioware.types.typeConverter.FullConverter;

public class ConnectionUtil {
	public static final int INPUT_PORT_POS=0;
	public static final int SIMULT_CONNS_POS=INPUT_PORT_POS+4;
	public static final int CURRENT_CONNS_POS=SIMULT_CONNS_POS+4;
	
	public static final int BUFF_SIZE=Integer.BYTES*3;
	
	public static final int CLOSED_SERVER=-1;
	
	public static void setCloseServerData(byte[] buffer) {
		FullConverter.toBytes(buffer,INPUT_PORT_POS,CLOSED_SERVER);
	}
	public static boolean isClosedServer(byte[] buffer) {
		return FullConverter.toInt(buffer,INPUT_PORT_POS)==CLOSED_SERVER;
	}
	public static void setNewBufferData(DistributedServer worker,byte[] buffer) {
		FullConverter.toBytes(buffer,INPUT_PORT_POS,worker.getServSocket().getInputPort());
		FullConverter.toBytes(buffer,SIMULT_CONNS_POS,worker.getMaxSimultaneousConnections());
		FullConverter.toBytes(buffer,CURRENT_CONNS_POS,worker.getCurrentConnections());
	}
	public static void setCurrentBufferData(DistributedServer worker,byte[] buffer) {
		FullConverter.toBytes(buffer,CURRENT_CONNS_POS,worker.getCurrentConnections());
	}
	public static void getCurrentBufferData(TcpActiveMachine machine,byte[] buffer) {
		machine.setCurrentConnections(FullConverter.toInt(buffer,CURRENT_CONNS_POS));
	}
	public static TcpActiveMachine getNewBufferData(byte[] buffer) {
		int port,simultConn,currentConns;
		TcpActiveMachine activeMachine;
		port=FullConverter.toInt(buffer,INPUT_PORT_POS);
		simultConn=FullConverter.toInt(buffer,SIMULT_CONNS_POS);
		currentConns=FullConverter.toInt(buffer,CURRENT_CONNS_POS);
		activeMachine= new TcpActiveMachine(port);
		activeMachine.setCurrentConnections(currentConns);
		activeMachine.setMaxConnections(simultConn);
		return activeMachine;
	}
	public static void getNewBufferData(TcpActiveMachine activeMachine,byte[] buffer) {
		activeMachine.setInputPort(FullConverter.toInt(buffer,INPUT_PORT_POS));
		activeMachine.setCurrentConnections(FullConverter.toInt(buffer,CURRENT_CONNS_POS));
		activeMachine.setMaxConnections(FullConverter.toInt(buffer,SIMULT_CONNS_POS));
	}
}	 
