package com.daioware.net.distributed;

public class UdpMachine extends TcpMachine{
	private int outputPort;
	
	public UdpMachine(int inputPort,int outputPort) {
		this("0.0.0.0",inputPort,outputPort);
	}
	public UdpMachine(String addr, int inputPort, int outputPort) {
		super(addr, inputPort);
		setOutputPort(outputPort);
	}

	public int getOutputPort() {
		return outputPort;
	}

	public void setOutputPort(int outputPort) {
		this.outputPort = outputPort;
	}
	
	
}
