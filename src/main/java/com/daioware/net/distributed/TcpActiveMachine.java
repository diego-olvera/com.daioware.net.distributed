package com.daioware.net.distributed;

public class TcpActiveMachine extends TcpMachine{
	private int currentConnections;
	private int maxConnections;
	
	public TcpActiveMachine() {
		this(0);
	}
	public TcpActiveMachine(int inputPort) {
		super(inputPort);
	}

	public TcpActiveMachine(String addr, int inputPort) {
		super(addr, inputPort);
	}
	
	public int getRemainingConnections() {
		return getMaxConnections()-currentConnections;
	}
	public int getCurrentConnections() {
		return currentConnections;
	}

	public void setCurrentConnections(int currentConnections) {
		this.currentConnections = currentConnections;
	}

	public int getMaxConnections() {
		return maxConnections;
	}

	public void setMaxConnections(int maxConnections) {
		this.maxConnections = maxConnections;
	}

	@Override
	public String toString() {
		return "TcpActiveMachine [currentConnections=" + getCurrentConnections() + ", addr=" + getAddr()
				+ ", inputPort=" + getInputPort() + "]";
	}
	
	
}
