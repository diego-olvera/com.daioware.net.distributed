package com.daioware.net.distributed;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.daioware.net.socket.GenericTCPServer;
import com.daioware.net.socket.TCPClientSocket;
import com.daioware.net.socket.TCPServSocket;
import com.daioware.net.socket.UDPElement;
import com.daioware.net.socket.UDPReceiver;
import com.daioware.net.socket.UDPSender;
import com.daioware.stream.Printer;

public abstract class DistributedServer extends GenericTCPServer implements Iterable<TcpActiveMachine>{
	
	private UDPElement udpElement;
	private Map<String, TcpActiveMachine> machines;
	private ConnectionsReceiver connReceiver;
	private Thread connReceiverThread;
	private ConnectionsSender connSender;
	private Thread connSenderThread;
	private ConnectionAnnouncer connAnnouncer;
	private Thread connAnnouncerThread;
	

	public DistributedServer(UDPElement udpElement,TCPServSocket servSocket, Printer printer, 
			int maxSimultaneousConnections) {
		super(servSocket, printer, maxSimultaneousConnections);
		setUdpElement(udpElement);
		initialize();
	}
	public DistributedServer(DistributedServerConfig config) throws UnknownHostException, IOException {
		super(config.getServSocket(),config.getPrinter(),1);
		initialize();
		UDPElement element=new UDPElement(new UDPSender(config.getUdpOutputPort()),
				new UDPReceiver(config.getUdpInputPort()));
		setUdpElement(element);
		for(TcpActiveMachine machine:config.getServers()) {
			add(machine);
		}		
	}
	protected void initialize() {
		machines=Collections.synchronizedMap(new HashMap<String,TcpActiveMachine>());
		connReceiver=new ConnectionsReceiver(this);
		connSender=new ConnectionsSender(this);
		connAnnouncer=new ConnectionAnnouncer(this);
	}
	public void announcePresence() {
		connAnnouncer.setAnnouncePresence(true);
	}
	
	public boolean isAnnouncePresence() {
		return connAnnouncer.isAnnouncePresence();
	}
	public UDPElement getUdpElement() {
		return udpElement;
	}
	public void setUdpElement(UDPElement udpElement) {
		this.udpElement = udpElement;
	}
	public TcpActiveMachine get(String addr) {
		return machines.get(addr);
	}
	public TcpActiveMachine remove(String addr) {
		return machines.remove(addr);
	}
	public boolean add(TcpActiveMachine a) {
		String newAddr=a.getAddr();
		String myAddr=getServSocket().getAddr();
		if(!myAddr.equals(newAddr)) {
			println("Added "+a);
			machines.put(newAddr,a);
			return true;
		}
		return false;
	}
	public TcpActiveMachine getActiveMachineWithBetterAvailableConnections() {
		TcpActiveMachine lowest=null;
		for(TcpActiveMachine machine:machines.values()){
			if(lowest==null) {
				lowest=machine;
			}
			else if(lowest.getRemainingConnections()>machine.getRemainingConnections()) {
				lowest=machine;
			}
			//else lowest remains the same
		}
		return lowest.getRemainingConnections()>=1?lowest:null;
	}
	
	protected Thread getNewClientThread(TCPClientSocket clientSocket) {
		return new WorkerClientThread(clientSocket,getRunnable(clientSocket),this);
	}
	@Override
	public abstract Runnable getRunnable(TCPClientSocket clientSocket);

	@Override
	public Iterator<TcpActiveMachine> iterator() {
		return new ArrayList<TcpActiveMachine>(machines.values()).iterator();
	}
	protected void begin() {
		(connReceiverThread=new Thread(connReceiver)).start();
		(connSenderThread=new Thread(connSender)).start();
		(connAnnouncerThread=new Thread(connAnnouncer)).start();
	}
	public synchronized void sendCurrentConnections() {
		synchronized (connSender) {
			connSender.notify();
		}
	}
	protected boolean readyToCloseLog() {
		return !connSenderThread.isAlive() && !connAnnouncerThread.isAlive()
				&& !connReceiverThread.isAlive();
	}
	public void close() throws IOException {
		println("Closing inner threads");
		connAnnouncer.setActive(false);
		connReceiver.setActive(false);
		connSender.setActive(false);
		println(getName()+" terminated");
		getPrinter().close();
		super.close();
	}
}
