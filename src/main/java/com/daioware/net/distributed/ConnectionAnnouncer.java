package com.daioware.net.distributed;

import static com.daioware.net.distributed.ConnectionUtil.*;

import java.io.IOException;

import com.daioware.net.socket.UDPSender;

public class ConnectionAnnouncer extends ConnectionManager{
	
	private boolean announcePresence=false;
	
	public ConnectionAnnouncer(DistributedServer worker) {
		super(worker);
	}

	public boolean isAnnouncePresence() {
		return announcePresence;
	}

	public synchronized void setAnnouncePresence(boolean b) {
		announcePresence=b;
		if(announcePresence) {
			notify();
		}
	}

	public synchronized void run() {
		println("ConnectionAnnouncer is running");
		UDPSender sender=worker.getUdpElement().getSender().clone();
		byte buffer[]=new byte[BUFF_SIZE];
		sender.setSrcAddr(worker.getServSocket().getAddr());
		while(isActive()) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
				println("ERROR:"+e.getMessage());
			}
			try {
				println("Announcing presence to co-workers..");
				for(TcpActiveMachine machine:worker) {
					sender.setDestAddr(machine.getAddr());
					ConnectionUtil.setNewBufferData(worker, buffer);
					sender.send(buffer);
				}
				println("Finished announcing presence");
			} catch (IOException e) {
				e.printStackTrace();
				println("ERROR:"+e.getMessage());
			}
		}
		notify();
		println("ConnectionAnnouncer stop running");
	}

}
