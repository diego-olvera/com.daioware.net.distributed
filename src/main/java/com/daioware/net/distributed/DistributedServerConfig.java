package com.daioware.net.distributed;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.daioware.file.FileUtil;
import com.daioware.net.socket.TCPServSocket;
import com.daioware.stream.Printer;
import com.daioware.stream.PrinterFile;
import com.daioware.stream.PrinterFileWithDate;


/**
 * @author Diego Olvera
 *
 */
public class DistributedServerConfig {
	public static final String PRINTER="printer";
	public static final String PRINTER_DATE="printerDate";
	public static final String PRINTER_FILE="printerFile";
	public static final String PRINTER_FILE_DATE="printerFileDate";
	public static final String printerTags[]= {PRINTER,PRINTER_DATE,PRINTER_FILE,PRINTER_FILE_DATE};

	private TCPServSocket servSocket;
	private int maxCurrentConn;
	private String name;
	private int udpInputPort;
	private int udpOutputPort;
	private Printer printer;
	private ArrayList<TcpActiveMachine> servers=new ArrayList<TcpActiveMachine>();
	
	
	public int getMaxCurrentConn() {
		return maxCurrentConn;
	}
	public void setMaxCurrentConn(int maxCurrentConn) {
		this.maxCurrentConn = maxCurrentConn;
	}
	public TCPServSocket getServSocket() {
		return servSocket;
	}
	public void setServSocket(TCPServSocket servSocket) {
		this.servSocket = servSocket;
	}
	public int getUdpInputPort() {
		return udpInputPort;
	}
	public void setUdpInputPort(int udpInputPort) {
		this.udpInputPort = udpInputPort;
	}
	
	public int getUdpOutputPort() {
		return udpOutputPort;
	}
	public void setUdpOutputPort(int udpOutputPort) {
		this.udpOutputPort = udpOutputPort;
	}
	public ArrayList<TcpActiveMachine> getServers() {
		return servers;
	}
	public void setServers(ArrayList<TcpActiveMachine> servers) {
		this.servers = servers;
	}
	public void setPrinter(Printer p) {
		printer=p;
	}
	public Printer getPrinter() {
		return printer;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void addServer(TcpActiveMachine act) {
		servers.add(act);
	}
	
	@Override
	public String toString() {
		return "DistributedServerConfig [servSocket=" + servSocket + ", maxCurrentConn=" + maxCurrentConn + ", name="
				+ name + ", udpInputPort=" + udpInputPort + ", udpOutputPort=" + udpOutputPort + ", servers=" + servers
				+ "]";
	}
	protected static TcpActiveMachine getMachine(File file,Element server) throws ParserConfigurationException, SAXException, IOException {
		String newFileStr=file.getParentFile().getAbsolutePath()+File.separator+
				((Element)server.getElementsByTagName("file").item(0))
				.getTextContent();
		TcpActiveMachine activeMachine=new TcpActiveMachine();
		File newFile=new File(newFileStr);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(newFile);
		doc.getDocumentElement().normalize();
		Element myServer = (Element) doc.getElementsByTagName("myServer").item(0);
		Element xmlSocket;
		try {
			xmlSocket=(Element) myServer.getElementsByTagName("tcpSocket").item(0);
		}catch(NullPointerException e) {
			xmlSocket=(Element) myServer.getElementsByTagName("tcpSecureSocket").item(0);
		}
		activeMachine.setAddr(((Element)xmlSocket.getElementsByTagName("address").item(0)).getTextContent());
		activeMachine.setInputPort(
				Integer.parseInt(
						((Element)xmlSocket.getElementsByTagName("port").item(0))
						.getTextContent()));
		activeMachine.setMaxConnections(
				Integer.parseInt(
						((Element)xmlSocket.getElementsByTagName("maxCurrentConn").item(0))
						.getTextContent()));
		return activeMachine;
	}
	

	protected static Printer getPrinter(File file,Element xmlServ) {
		Element element=null;
		String path=file.getParentFile().getAbsolutePath()+File.separator;
		for(String tag:printerTags) {
			try {
				element=(Element)xmlServ.getElementsByTagName(tag).item(0);
				if(element!=null) {
					break;
				}
			}catch(Exception e) {
			}
		}
		if(element!=null) {
			try {
				switch(element.getTagName()) {
					case PRINTER:return Printer.defaultPrinter;
					case PRINTER_DATE:return Printer.defaultPrinterWithDate;
					case PRINTER_FILE_DATE:return new PrinterFileWithDate(
							FileUtil.convertPathToCompatiblePath(
									path+element.getTextContent().trim()));
					case PRINTER_FILE:return new PrinterFile(
							FileUtil.convertPathToCompatiblePath(
									path+element.getTextContent().trim()));
				}
			}catch(IOException e) {
				e.printStackTrace();
			}			
		}
		return Printer.emptyPrinter;
	}
	public static DistributedServerConfig getConfig(File file) throws SAXException, IOException, ParserConfigurationException {
		DistributedServerConfig config=new DistributedServerConfig();
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(file);
		doc.getDocumentElement().normalize();
		NodeList list=doc.getElementsByTagName("myServer");
		Element myServer = (Element) list.item(0);
		Element udp=((Element)myServer.getElementsByTagName("udp").item(0));
		Element xmlSocket;
		TCPServSocket servSocket;
		int port,maxConn,maxIncomingConn;
		String addr;
		boolean isSecureSocket;
	 
		xmlSocket=(Element) myServer.getElementsByTagName("tcpSocket").item(0);
		if(xmlSocket==null) {
			xmlSocket=(Element) myServer.getElementsByTagName("tcpSecureSelfSignedSocket").item(0);
			isSecureSocket=true;
		}
		else {
			isSecureSocket=false;
		}
		addr=((Element)xmlSocket.getElementsByTagName("address").item(0)).getTextContent();
		port=Integer.parseInt(
						((Element)xmlSocket.getElementsByTagName("port").item(0))
						.getTextContent());
		maxConn=
				Integer.parseInt(
						((Element)xmlSocket.getElementsByTagName("maxCurrentConn").item(0))
						.getTextContent());
		config.setMaxCurrentConn(maxConn);
		maxIncomingConn=
				Integer.parseInt(
						((Element)xmlSocket.getElementsByTagName("maxIncomingConn").item(0))
						.getTextContent());
		config.setName(
						((Element)xmlSocket.getElementsByTagName("name").item(0))
						.getTextContent());
		
		config.setPrinter(getPrinter(file,myServer));

		
		if(isSecureSocket) {
			servSocket=getSecureSocket(file,port,xmlSocket);
		}
		else {
			servSocket=new TCPServSocket(addr, port, maxIncomingConn);
		}
		config.setServSocket(servSocket);
		config.setUdpInputPort(
				Integer.parseInt(
						((Element)udp.getElementsByTagName("inputPort").item(0))
						.getTextContent()));
		config.setUdpOutputPort(
				Integer.parseInt(
						((Element)udp.getElementsByTagName("inputPort").item(0))
						.getTextContent()));
		NodeList servers;
		servers=myServer.getElementsByTagName("servers");
		for(int i=0,j=servers.getLength();i<j;i++) {
			config.addServer(getMachine(file,(Element) servers.item(i)));
		}
		return config;
	}
	
	private static TCPServSocket getSecureSocket(File file,int port,Element xmlSocket) throws UnknownHostException, IOException {
		String path=file.getParentFile().getAbsolutePath()+File.separator;
		String keyStore=xmlSocket.getElementsByTagName("keyStoreFile").item(0).getTextContent();
		String completePath=FileUtil.convertPathToCompatiblePath(path+keyStore);
		String pwd=xmlSocket.getElementsByTagName("keyStorePassword").item(0).getTextContent();
		return new TCPServSocket(new File(completePath),pwd,port);
	}	
}
