package com.daioware.net.distributed;

public class TcpMachine {
	private String addr;
	private int inputPort;
		
	public TcpMachine(int inputPort) {
		this("0.0.0.0",inputPort);
	}

	public TcpMachine(String addr, int inputPort) {
		setAddr(addr);
		setInputPort(inputPort);
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public int getInputPort() {
		return inputPort;
	}

	public void setInputPort(int inputPort) {
		this.inputPort = inputPort;
	}
}
