package com.daioware.net.distributed;

import java.io.IOException;

import com.daioware.net.socket.ClientThread;
import com.daioware.net.socket.TCPClientSocket;
import com.daioware.types.typeConverter.FullConverter;

public class WorkerClientThread extends ClientThread{
	
	protected Runnable target;
	public WorkerClientThread(TCPClientSocket socket, Runnable r, DistributedServer server) {
		super(socket, r, server);
		target=r;
	}
	public void run() {
		int currentConn;
		String completeAddr=socket.getCompleteInputAddr();
		println("Client socket connected with addr:"+completeAddr);
		TcpActiveMachine machine;
		DistributedServer server=(DistributedServer) this.server;
		byte message[];
		String addr;
		int messagePos;
		if((currentConn=server.getCurrentConnections())<server.getMaxSimultaneousConnections()) {
			server.incrementRunningThreads();
			println("Current clients:"+(server.getCurrentConnections()));
			server.sendCurrentConnections();
			addr=server.getServSocket().getAddr();
			println("Confirming connection to addr:"+addr);
			messagePos=Integer.BYTES+addr.length();
			message=new byte[messagePos+Integer.BYTES];
			FullConverter.toBytes(message,0,addr);
			FullConverter.toBytes(message,messagePos,server.getServSocket().getInputPort());
			try {
				socket.write(message);
			} catch (IOException e) {
				e.printStackTrace();
			}
			println("Confirming connection terminated");
			server.addSession(this);
			target.run();
			server.removeSession(this);
			try {
				socket.close();
			} catch (IOException e) {
			}
			server.decrementRunningThreads();
		}
		else {
			println("Current clients "+currentConn+" is equals to the limit "+
					server.getMaxSimultaneousConnections());
			println("Getting server with lowest simultaneous connections...");
			machine=server.getActiveMachineWithBetterAvailableConnections();
			if(machine!=null) {
				println("Redirecting to machine:\n"+machine);
				addr=machine.getAddr();
				messagePos=Integer.BYTES+addr.length();
				message=new byte[messagePos+Integer.BYTES];
				FullConverter.toBytes(message,0,addr);
				FullConverter.toBytes(message,messagePos,machine.getInputPort());
				println("Sending message");
				try {
					socket.write(message);
					println("Redirect message Sent correctly");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			else {
				println("Didn't found any possible new connection");
			}
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}	
}
